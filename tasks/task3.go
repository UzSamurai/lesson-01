package main

import (
	"fmt"
)

func main() {
	var r float32 = 10.04

	// fmt.Scanf("%f", &r)
	fmt.Println("R =", r)

	fmt.Printf("Area: %0.2f\n", area(r))
}

func area(r float32) (area float32) {
	  var P float32 = 3.141592653589
	S := 4*r*r

	 C := P*r*r

	area = S - C

	return area
	
}
